import React from 'react';
import ReactImg from '../Images/react-icon-small.png'

export default function Nav(props) {
    return (
        <nav className={props.darkMode ? "dark" : ""}>
            <div className='left--side'>
                <img className='react--img' src={ReactImg} alt='React-Img' />
                <h4 className='react--facts'>ReactFacts</h4>
            </div>
            <div className='right--side'>
                <p className='toggler--light'>Light</p>
                <div className="toggler--slider" onClick={props.tooglerDarkMode}>
                    <div className="toggler--slider--circle"></div>
                </div>
                <p className='toggler--dark'>Dark</p>
            </div>
        </nav>
    )
}
