import './App.css';
import Main from './components/Main';
import Nav from './components/Nav';
import React from 'react'

function App() {

  const [darkMode, setDarkMode] = React.useState(false)

  function handleDarkMode() {
    setDarkMode(!darkMode)
  }

  return (
    <div className="container">
      <Nav tooglerDarkMode={handleDarkMode} darkMode={darkMode} />
      <Main darkMode={darkMode} />
    </div>
  );
}

export default App;
